module Homunculus.Types where

data Config = Conf
  { diceStrs :: [String]
  , showNames :: Bool
  , showDice :: Bool
  , showGens :: Bool
  , showChars :: Bool
  }
  deriving (Eq, Show)

data Generator = Generator
  { genTitle :: String
  , tables :: [Table]
  , options :: [Property]
  }
  deriving (Eq, Show)

instance Ord Generator where
  g1 `compare` g2 = (genTitle g1) `compare` (genTitle g2)

data Table = Table
  { tabTitle :: String
  , tabLines :: [Line]
  }
  deriving (Eq, Show)

type Property = (String, [String])

type Line = (Int, String)

-- Token in the parsing sense, not the gaming sense
data DiceToken
  = TokenSum [Int]
  | TokenCall (String, String)
  | TokenElse String
  deriving (Eq, Show)

data RPGCharacter = RPGChar
  { charName :: String
  , --  We keep charFields a String to allow for easier editing and to
    --  not restrict users who aren't interested in the dice roller
    --  integration. We'll parse the String to [RPGField] as needed.
    charFields :: String
  , charNotes :: String
  }
  deriving (Show, Eq)

type RPGField = String
