{-# LANGUAGE OverloadedStrings #-}

module Homunculus.IO.Monomer.Util where

import Control.Lens
import Data.List (delete)
import Monomer
import Text.Wrap

import qualified Data.Text as T
import qualified Monomer.Lens as L

import Homunculus.IO.Monomer.Types

-- vgrid makes for awkward layouts but hgrid is really handy
reactContainer :: Bool -> [YHHNode] -> YHHNode
reactContainer b = if b then vstack else hgrid

reactStack :: Bool -> [YHHNode] -> YHHNode
reactStack b = if b then vstack else hstack

actionRowBox :: YHHNode -> YHHEvent -> [StyleState] -> YHHNode
actionRowBox row ev styles =
  box_ [onClick ev] row
    `styleBasic` styles

titledWidgets :: T.Text -> [YHHNode] -> YHHNode
titledWidgets title nodes =
  vstack $
    (label title `styleBasic` [textSize 12, textColor lightGray])
      : nodes

delBox :: Bool -> String -> YHHEvent -> (YHHModel -> YHHModel) -> YHHNode
delBox True str pushEvent toggleModel =
  hstack
    [ spacer
    , label (T.pack "Are you sure?")
    , tooltip (T.pack $ str ++ "!") $
        rowIconAlertStyle $
          button remixDeleteBinFill pushEvent
    , tooltip "Cancel" $
        rowIconStyle $
          button
            remixCloseFill
            (EvAction True toggleModel)
    ]
    `styleBasic` [bgColor $ black, radius 10]
delBox False str _ toggledModel =
  tooltip (T.pack str) $
    rowIconStyle $
      button
        remixDeleteBinFill
        (EvAction False toggledModel)

margin :: Double -> YHHNode -> YHHNode
margin i w = (box w) `styleBasic` [padding i]

wrapBy :: YHHWenv -> Int -> T.Text -> T.Text
wrapBy wenv i = wrapText defaultWrapSettings width
 where
  width = (floor $ _sW $ wenv ^. L.windowSize) `div` i

boxStyle :: [StyleState]
boxStyle = [padding 2, border 1 darkGray]

visTextStyle :: [StyleState]
visTextStyle = [textSize 12, padding 2]

frameStyle :: [StyleState]
frameStyle = [bgColor $ black & L.a .~ 0.4, padding 5]

popupStyle :: [StyleState]
popupStyle = [bgColor $ black & L.a .~ 0.9, radius 10]

titleStyle :: [StyleState]
titleStyle = [paddingL 1, textColor lightGray]

inputStyle :: [StyleState]
inputStyle = [padding 3, bgColor $ black & L.a .~ 0.2]

rowIconStyle :: (CmbStyleHover t, CmbStyleBasic t) => t -> t
rowIconStyle = (flip styleBasic) b . (flip styleHover) h
 where
  b =
    [ textColor gray
    , bgColor transparent
    , border 0 transparent
    , padding 1
    , paddingT 10
    , textFont "Icon"
    ]
  h = [textColor lightGray]

rowIconInactiveStyle :: (CmbStyleHover t, CmbStyleBasic t) => t -> t
rowIconInactiveStyle =
  (flip styleBasic) x
    . (flip styleHover) x
    . rowIconStyle
 where
  x = [textColor $ gray & L.a .~ 0.5]

rowIconAlertStyle :: (CmbStyleHover t, CmbStyleBasic t) => t -> t
rowIconAlertStyle =
  (flip styleBasic) b
    . (flip styleHover) h
    . rowIconStyle
 where
  b = [textColor $ red & L.a .~ 0.5]
  h = [textColor red]

footerButtonStyle :: [StyleState]
footerButtonStyle =
  [ textColor lightGray
  , textSize 12
  , bgColor $ black & L.a .~ 0.2
  , border 1 darkGray
  ]

remixDie :: T.Text
remixDie = toGlyph 0XF400

rowHint :: T.Text
rowHint = "Enter result likelihood to the left and result text here"

optionHint :: T.Text
optionHint = "Comma,Separated,List,With,No,Spaces"

statHint :: T.Text
statHint = "Stat Val : Comment, ..."

diceHelp :: T.Text
diceHelp =
  "Standard dice syntax is supported.\n"
    <> "For example: 3d6, 1d100, 1d8+3\n\n"
    <> "Numeric character stats can also be used\n"
    <> "with [CharName].[StatName] syntax.\n"
    <> "For example: 1d20 + Strider.DexMod"

tableHelp :: T.Text
tableHelp =
  "Each table is a list of rows which are checked when the\n"
    <> "table is used. A row's weight is the likelihood (divided by\n"
    <> "the total weight) that that row's text is chosen.\n\n"
    <> "The chosen row's text is parsed for these commands:\n\n"
    <> "{table:name}\n"
    <> "    Return a result from table 'name'. Recursion is NOT\n"
    <> "    supported at this time; tables are discarded after each\n"
    <> "    call, so if the system has already generated a result for\n"
    <> "    that table (or is in the process of doing so) it will\n"
    <> "    return a not-found error instead.\n"
    <> "{property:val1=result1;val2=result2;else=result3}\n"
    <> "    Return a result string based on what options were set.\n"
    <> "    The values checked against must exactly match the\n"
    <> "    field within the generator's options. Only one result will\n"
    <> "    be returned so e.g. result2 will only appear if val2 is\n"
    <> "    present AND val1 is not. If none of the values are\n"
    <> "    present the text in the else portion will be used.\n"
    <> "{list:a|b|c}\n"
    <> "    Return either a, b, or c at random (equal chance).\n"
    <> "{br}\n"
    <> "    Insert newline.\n"
    <> "{upper}\n"
    <> "    Up-case the letter immediately following.\n"
    <> "{lower}\n"
    <> "    Down-case the letter immediately following.\n\n"
    <> "Dice syntax may also be used, but not character calls."

emptyText :: T.Text
emptyText =
  "You look at the screen in front of you and see a homunculus "
    <> "grinning back. It wants nothing more than to help you with "
    <> "your next game.\n\nWhat do you do?"

checkTEdit :: GenEditFlag -> Int -> Bool
checkTEdit (EdTable x) y = x == y
checkTEdit _ _ = False

checkOEdit :: GenEditFlag -> Int -> Bool
checkOEdit (EdProperty x) y = x == y
checkOEdit _ _ = False

checkNEdit :: GenEditFlag -> Bool
checkNEdit (EdName) = True
checkNEdit _ = False

getNamesError :: YHHModel -> String
getNamesError m = f $ m ^. visFlags
 where
  f [] = ""
  f (x : xs) = case x of
    VisNameError s -> s
    _ -> f xs

showDiceDelete :: Int -> YHHModel -> Bool
showDiceDelete = getVisFlag . VisDelDice

showGenDelete :: YHHModel -> Bool
showGenDelete = getVisFlag VisDelGen

showGenResults :: YHHModel -> Bool
showGenResults = getVisFlag VisGenResults

showTabDelete :: Int -> YHHModel -> Bool
showTabDelete = getVisFlag . VisDelTab

showOptDelete :: Int -> YHHModel -> Bool
showOptDelete = getVisFlag . VisDelOpt

showExpandedChar :: Int -> YHHModel -> Bool
showExpandedChar = getVisFlag . VisCharExp

showCharDelete :: Int -> YHHModel -> Bool
showCharDelete = getVisFlag . VisDelChar

getVisFlag :: VisFlag -> YHHModel -> Bool
getVisFlag f m = f `elem` (m ^. visFlags)

toggleNameDelete :: Int -> YHHModel -> YHHModel
toggleNameDelete = toggleVisFlag . VisDelName

toggleDiceDelete :: Int -> YHHModel -> YHHModel
toggleDiceDelete = toggleVisFlag . VisDelDice

toggleGenDelete :: YHHModel -> YHHModel
toggleGenDelete = toggleVisFlag VisDelGen

toggleWidgetGenResults :: YHHModel -> YHHModel
toggleWidgetGenResults = toggleVisFlag VisGenResults

toggleTabDelete :: Int -> YHHModel -> YHHModel
toggleTabDelete = toggleVisFlag . VisDelTab

toggleOptDelete :: Int -> YHHModel -> YHHModel
toggleOptDelete = toggleVisFlag . VisDelOpt

toggleCharDelete :: Int -> YHHModel -> YHHModel
toggleCharDelete = toggleVisFlag . VisDelChar

toggleExpandedChar :: Int -> YHHModel -> YHHModel
toggleExpandedChar = toggleVisFlag . VisCharExp

toggleVisFlag :: VisFlag -> YHHModel -> YHHModel
toggleVisFlag flag model = model & visFlags %~ f
 where
  f = if getVisFlag flag model then delete flag else (:) flag

addVisFlag :: VisFlag -> YHHModel -> YHHModel
addVisFlag flag model =
  if getVisFlag flag model then model else model & visFlags %~ (:) flag

delVisFlag :: VisFlag -> YHHModel -> YHHModel
delVisFlag flag model = model & visFlags %~ delete flag

removeStaleFlags :: YHHModel -> YHHModel
removeStaleFlags m = m & visFlags %~ filter f
 where
  f (VisDelName _) = False
  f (VisDelDice _) = False
  f (VisDelTab _) = False
  f (VisDelOpt _) = False
  f (VisNameError _) = False
  f VisDelGen = False
  f (VisDelChar _) = False
  f VisDiceHelp = False
  f VisTableHelp = False
  f _ = True

removeGenEditFlags :: YHHModel -> YHHModel
removeGenEditFlags m = m & visFlags %~ filter f
 where
  f (VisGenEdit _) = False
  f _ = True

isJust :: Maybe a -> Bool
isJust Nothing = False
isJust _ = True
