{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Homunculus.IO.Monomer.Types where

import Control.Lens (makeLenses)
import Data.Text (Text)
import Monomer
import System.Random (StdGen)

import Homunculus.Types

type NameList = (FilePath, Text, [Text])

type NameListData =
  Maybe (FilePath, Text, Int)

type LineData = Maybe (Int, String, Int, Int, Bool)

type Dice = (String, String)

type PseudoGen = (String, [Table], [Property], [(Int, String)])

type YHHWenv =
  WidgetEnv YHHModel YHHEvent

type YHHNode =
  WidgetNode YHHModel YHHEvent

type YHHResp =
  AppEventResponse YHHModel YHHEvent

data VisFlag
  = VisTray
  | VisNames
  | VisNameError String
  | VisDice
  | VisGens
  | VisGenResults
  | VisChars
  | VisDelName Int
  | VisDelDice Int
  | VisDelGen
  | VisDelTab Int
  | VisDelOpt Int
  | VisDelChar Int
  | VisGenSave
  | VisCharExp Int
  | VisDiceHelp
  | VisTableHelp
  | VisGenEdit GenEditFlag
  deriving (Eq, Show)

data GenEditFlag = EdTable Int | EdProperty Int | EdName
  deriving (Eq, Show)

data YHHModel = YHHModel
  -- BACKGROUND DATA
  { _dataPath :: FilePath
  , _dataPaths :: [FilePath]
  , -- if vertical==False, horizontal==True
    _alignVertical :: Bool
  , _namesBoxData :: NameListData
  , _propSelections :: [(Int, String)]
  , _lineBoxData :: LineData
  , _genResults :: Text
  , _visFlags :: [VisFlag]
  , -- CONTENT
    _reaction :: Maybe String
  , _images :: [[FilePath]]
  , _nameLists :: [NameList]
  , _dice :: [Dice]
  , _gen :: Generator
  , -- Actually just the titles of found gens
    _gens :: [String]
  , _characters :: [RPGCharacter]
  }
  deriving (Eq, Show)

data YHHEvent
  = EvNull
  | EvPathChange FilePath
  | -- Absorb a () Task then IO reread & reload the current model
    EvRefresh ()
  | EvReload Bool YHHModel
  | EvAction Bool (YHHModel -> YHHModel)
  | EvSave VisFlag
  | EvReactGen
  | EvReaction StdGen
  | EvShuffleAll
  | EvShuffleImage Int
  | EvSetImages (Maybe Int) StdGen
  | EvNameGen Int
  | EvName Int StdGen
  | EvNameBox NameListData
  | EvNameBoxSave NameListData
  | EvNameListsNew FilePath
  | EvRollGen Int
  | EvRollDel Int
  | EvRollNew
  | EvRollChange Int Text
  | EvRoll Int StdGen
  | EvGenerate Table
  | EvGenRun Table StdGen
  | EvGenLoad String
  | EvGenChange (Maybe Generator)
  | -- Works like EvReload but lights up the gen box save button too
    EvGenEdits [(YHHModel -> YHHModel)]
  | EvGenSave
  | EvGenDelete
  | EvCharDelete Int
  | EvCharsEdit (YHHModel -> YHHModel)

makeLenses 'YHHModel

nlPath :: NameList -> FilePath
nlPath (fp, _, _) = fp

nlName :: NameList -> Text
nlName (_, n, _) = n

nlList :: NameList -> [Text]
nlList (_, _, ns) = ns

ldWeight :: LineData -> Int
ldWeight Nothing = (-1)
ldWeight (Just (w, _, _, _, _)) = w

ldString :: LineData -> String
ldString Nothing = ""
ldString (Just (_, s, _, _, _)) = s

ldTable :: LineData -> Int
ldTable Nothing = (-1)
ldTable (Just (_, _, n, _, _)) = n

ldLine :: LineData -> Int
ldLine Nothing = (-1)
ldLine (Just (_, _, _, n, _)) = n

ldExpand :: LineData -> Bool
ldExpand Nothing = False
ldExpand (Just (_, _, _, _, b)) = b
