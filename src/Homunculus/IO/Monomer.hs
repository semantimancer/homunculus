{-# LANGUAGE OverloadedStrings #-}

module Homunculus.IO.Monomer where

import Control.Lens
import Control.Monad (filterM)
import Data.List
import Data.List.Extra (replace)
import Data.List.Split (splitOn)
import Monomer
import System.Directory
import System.FilePath
import System.Random

import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Monomer.Lens as L

import Homunculus
import Homunculus.IO
import Homunculus.IO.Monomer.Types
import Homunculus.IO.Monomer.Util
import Homunculus.Parser (diceResult, tableResult)
import Homunculus.Types

monomerConfig :: FilePath -> [AppConfig s YHHEvent]
monomerConfig dataPath =
  [ appWindowTitle "Your Helpful Homunculus"
  , appTheme darkTheme
  , appFontDef "Regular" (T.pack $ assetDir </> "Roboto-Regular.ttf")
  , appFontDef "Icon" (T.pack $ assetDir </> "remixicon.ttf")
  , appInitEvent EvNull
  ]
 where
  assetDir = dataPath </> ".assets"

buildUI :: YHHWenv -> YHHModel -> YHHNode
buildUI wenv model = zstack [contentScreen, namesBoxScreen]
 where
  contentScreen =
    vstack
      [ hstack -- Header
          [ label "Your Helpful Homunculus"
          , filler
          , dropdown_
              dataPath
              (model ^. dataPaths)
              (\dir -> hstack [label "Active: ", spacer, label $ T.pack dir])
              (\dir -> label $ T.pack dir)
              [onChange EvPathChange]
              `styleBasic` [bgColor black, rangeWidth 500.0 700.0]
          , filler
          , tooltip "Toggle Tray" $
              rowIconStyle $
                button
                  (toGlyph $ if getVisFlag VisTray model then 0XF2EB else 0XF2E1)
                  (EvAction True $ toggleVisFlag VisTray)
          , spacer
          ]
          `styleBasic` frameStyle
      , widgetIf (getVisFlag VisTray model) $
          headerTray (model ^. images) (model ^. reaction)
      , reactStack -- Main Content
          (model ^. alignVertical)
          $ map
            (\xs -> reactContainer (not $ model ^. alignVertical) $ map contentWidget xs)
            widgetList'
      , filler
      , widgetIf
          (not $ foldr (\(_, f, _) acc -> acc || getVisFlag f model) False widgetList)
          $ box_ [alignCenter]
          $ label_ (wrapBy wenv 10 emptyText) [multiline]
      , filler
      , (flip styleBasic) -- Footer
          frameStyle
          $ hstack_ [childSpacing]
          $ (map visButton widgetList)
            ++ [ filler
               , toggleButton "Vertical Layout" alignVertical `styleBasic` visTextStyle
               ]
      ]

  contentWidget (_, f, w) = widgetIf (getVisFlag f model) (margin 5 $ box w `styleBasic` boxStyle)

  visButton (name, flag, _) =
    toggleButtonV name (getVisFlag flag model) (\_ -> EvSave flag)
      `styleBasic` visTextStyle

  widgetList = concat widgetList'

  widgetList' =
    [
      [ ("Names", VisNames, nameBox (model ^. visFlags) (model ^. nameLists))
      , ("Dice", VisDice, diceBox (model ^. visFlags) (model ^. dice))
      ]
    , [("Generators", VisGens, genBox)]
    , [("Characters", VisChars, charBox (model ^. visFlags) (model ^. characters))]
    ]

  -- Deliberately not using a popup here. They seem finicky to get working at the
  -- best of times (more "surface area" for bugs) and also I don't want someone
  -- accidentally clicking away after painstakingly entering names and losing all
  -- that progress. That said, the rangeWidth / rangeHeight make me nervous too.
  namesBoxScreen =
    widgetMaybe (model ^. namesBoxData) $
      \(fn, txt, idx) ->
        (flip styleBasic)
          [bgColor $ black & L.a .~ 0.4]
          $ box_ [alignCenter]
          $ (flip styleBasic)
            (frameStyle ++ [rangeWidth 400 600, rangeHeight 300 600, radius 5])
          $ vstack
            [ spacer
            , hstack
                [ label "Filename: " `styleBasic` [textColor darkGray]
                , spacer
                , textFieldV
                    (T.pack fn)
                    (\x -> EvNameBox $ Just (T.unpack x, txt, idx))
                , spacer
                , label ".list"
                , spacer
                ]
            , spacer
            , label "Names (one per line):" `styleBasic` [textColor darkGray]
            , spacer
            , textAreaV txt (\x -> EvNameBox $ Just (fn, x, idx))
            , spacer
            , hstack
                [ filler
                , button "Save" (EvNameBoxSave $ model ^. namesBoxData)
                , spacer
                , button "Cancel" (EvNameBox Nothing)
                , filler
                ]
            ]

  genBox =
    vstack
      [ spacer_ [width 2]
      , genBoxSelect (model ^. visFlags) (genTitle $ model ^. gen) (model ^. gens)
      , separatorLine `styleBasic` [padding 25, paddingT 10, paddingB 10]
      , reactContainer
          (not $ model ^. alignVertical)
          [ genBoxLeft
          , genBoxRight
          ]
      ]

  genBoxLeft =
    hstack
      [ spacer
      , vscroll $
          vstack
            [ genBoxTitle
                (model ^. visFlags)
                (model ^. gen)
            , spacer
            , genBoxOptions
                (model ^. visFlags)
                (model ^. propSelections)
                (model ^. gen)
            , spacer
            , genBoxTables
                (model ^. visFlags)
                (model ^. lineBoxData)
                (model ^. gen)
            , box_ [alignLeft] $
                toggleButtonV
                  "Show Results"
                  (showGenResults model)
                  (\_ -> EvAction True toggleWidgetGenResults)
            , spacer
            ]
      , spacer
      ]

  genBoxRight =
    widgetIf (showGenResults model) $
      vstack
        [ box (textArea genResults)
        , tooltip "Clear results" $
            flip styleBasic [paddingR 10] $
              box_ [alignRight] $
                if (model ^. genResults) /= ""
                  then
                    rowIconStyle $
                      button remixEraserFill (EvAction True $ genResults .~ "")
                  else rowIconInactiveStyle $ label remixEraserFill
        ]
        `styleBasic` [padding 5]

headerTray :: [[FilePath]] -> Maybe String -> YHHNode
headerTray imgs r =
  (flip styleBasic) frameStyle $
    hgrid
      [ filler
      , hstack
          [ filler
          , tooltip "Reroll All" $ rowIconStyle $ button remixDie EvShuffleAll
          , spacer
          , hgrid $ map makeIcon $ zip [0 ..] imgs
          , filler
          ]
      , hstack
          [ filler
          , actionRowBox reactBox EvReactGen []
          , spacer
          ]
      ]
 where
  makeIcon (i, x) =
    widgetIf (x /= []) $
      box_
        [onClick $ EvShuffleImage i]
        (image_ (T.pack $ head x) [fitWidth, fitHeight])
        `styleBasic` [width 36, height 36, padding 2]

  reactBox =
    hstack
      [ label $ reactText r
      , spacer
      , tooltip "React!" $ rowIconStyle $ label remixDie
      , spacer
      ]

  reactText Nothing = "-: No reaction roll yet"
  reactText (Just s) = T.pack s

nameBox :: [VisFlag] -> [NameList] -> YHHNode
nameBox vs nls = vstack $ [header, error] ++ content
 where
  header =
    hstack
      [ label "Names" `styleBasic` titleStyle
      , filler
      , tooltip "Add names file" $
          rowIconStyle $
            button remixAddCircleFill (EvNameListsNew "New.list")
      , spacer
      ]

  error =
    box_ [alignRight] (label $ getError vs)
      `styleBasic` [textColor $ red & L.a .~ 0.6, textSize 12, paddingR 15]

  getError [] = ""
  getError (x : xs) = case x of
    VisNameError s -> T.pack $ s
    _ -> getError xs

  content = zipWith (\i nl -> nameRow i nl) [0 ..] nls

  nameRow i nl =
    hstack
      [ spacer
      , tooltip "Edit name list" $
          rowIconStyle $
            button
              remixEdit2Fill
              (EvNameBox $ Just $ (nlPathNoSuffix nl, T.unlines $ nlList nl, i))
      , spacer
      , actionRowBox (nameRow' nl) (EvNameGen i) [border 3 transparent]
      , delBox
          ((VisDelName i) `elem` vs)
          "Delete name row"
          EvNull
          (toggleNameDelete i)
      , spacer
      ]

  nameRow' nl =
    hstack
      [ titledWidgets
          (T.pack $ take (length (nlPath nl) - 5) $ nlPath nl)
          [label ("Name: " <> nlName nl) `styleBasic` [paddingL 5]]
      , filler
      ]
      `styleBasic` [padding 2]
      `styleHover` [bgColor $ darkGray & L.a .~ 0.5]

  nlPathNoSuffix nl = let nl' = nlPath nl in take ((length nl') - 5) nl'

--
diceBox :: [VisFlag] -> [Dice] -> YHHNode
diceBox vs ds = vstack $ header : content
 where
  header =
    hstack
      [ label "Dice" `styleBasic` titleStyle
      , filler
      , popupV_
          (VisDiceHelp `elem` vs)
          (\b -> if b then EvNull else EvAction False $ toggleVisFlag VisDiceHelp)
          [alignLeft]
          (label_ diceHelp [multiline] `styleBasic` ((padding 10) : popupStyle))
      , tooltip "Help" $
          rowIconStyle $
            button remixQuestionFill (EvAction False $ toggleVisFlag VisDiceHelp)
      , spacer
      , tooltip "Add dice row" $
          rowIconStyle $
            button remixAddCircleFill EvRollNew
      , spacer
      ]

  content = intersperse separatorLine $ zipWith (\i d -> diceRow i d) [0 ..] ds

  diceRow i (str, res) =
    hstack
      [ (flip styleBasic) [paddingB 5, paddingT 5] $
          box $
            textFieldV
              (T.pack str)
              (EvRollChange i)
              `styleBasic` [rangeWidth 60.0 250.0, padding 5]
      , actionRowBox (diceRow' res) (EvRollGen i) []
      , spacer
      , delBox
          ((VisDelDice i) `elem` vs)
          "Delete dice row"
          (EvRollDel i)
          (\m -> m & toggleDiceDelete i)
      , spacer
      ]

  diceRow' res =
    hstack
      [ spacer
      , tooltip "Roll!" $ rowIconStyle $ label remixDie
      , filler
      , label_ (T.pack res) [multiline]
          `styleBasic` [textSize 14, minWidth 75.0]
      ]
      `styleBasic` [padding 1]
      `styleHover` [bgColor $ darkGray & L.a .~ 0.5]

genBoxSelect :: [VisFlag] -> String -> [String] -> YHHNode
genBoxSelect vs thisTitle genTitles =
  hstack
    [ label "Generators" `styleBasic` titleStyle
    , filler
    , dropdownV
        thisTitle
        (\_ t -> EvGenLoad t)
        genTitles
        (\ttl -> hstack [label "Selected: ", spacer, label $ T.pack ttl])
        (\ttl -> label $ T.pack ttl)
    , spacer
    , delBox
        (VisDelGen `elem` vs)
        "Delete generator"
        EvGenDelete
        toggleGenDelete
    , spacer
    , tooltip "Save generator" $
        if VisGenSave `elem` vs
          then rowIconStyle $ button remixSave2Fill EvGenSave
          else rowIconInactiveStyle $ label remixSave2Fill
    , filler
    , tooltip "New generator" $
        rowIconStyle $
          button
            remixAddCircleFill
            (EvAction True $ \m -> m & gen .~ Generator "New Generator" [] [])
    , spacer
    ]

genBoxTitle :: [VisFlag] -> Generator -> YHHNode
genBoxTitle vs g =
  box_ [alignCenter] $
    hstack
      [ widgetIf ((VisGenEdit EdName) `notElem` vs) $
          label (T.pack $ genTitle g)
      , -- TODO: Add the validInput Cfg with a function
        -- to ensure you get a valid filename
        widgetIf ((VisGenEdit EdName) `elem` vs) $
          textFieldV
            (T.pack $ genTitle g)
            (\n -> EvGenEdits [updateGen $ setGenTitle (T.unpack n)])
      , tooltip "Edit name" $
          rowIconStyle $
            -- Basically a toggle, but making it EdName can stop other Ed flags
            button remixEdit2Fill $
              EvAction True $
                toggleVisFlag (VisGenEdit EdName)
      ]

genBoxOptions :: [VisFlag] -> [(Int, String)] -> Generator -> YHHNode
genBoxOptions vs propSelects g =
  (flip styleBasic) boxStyle $
    vstack $
      [ hstack
          [ label "Properties" `styleBasic` titleStyle
          , filler
          , tooltip "New property" $
              rowIconStyle $
                button remixAddCircleFill $
                  EvGenEdits [updateGen addProperty]
          , spacer
          ]
      ]
        ++ zipWith (\i opt -> optRow i opt) [0 ..] (options g)
 where
  optRow i (n, os) =
    hstack
      [ optEdButton i
      , spacer
      , widgetIf (not $ oEditing i) $ label (T.pack n)
      , widgetIf (oEditing i) $
          textFieldV (T.pack n) $
            \t -> EvGenEdits [updateGen $ setPropTitle i (T.unpack t)]
      , spacer
      , (optRowVals i os) `nodeVisible` (not $ oEditing i)
      , (optRowEd i os) `nodeVisible` (oEditing i)
      , spacer
      , delBox
          ((VisDelOpt i) `elem` vs)
          "Delete option"
          (EvGenEdits [updateGen $ deleteProperty i])
          (toggleOptDelete i)
          `nodeVisible` (oEditing i)
      , spacer
      ]
      `styleBasic` [padding 2, paddingB 5]

  optRowVals i os =
    dropdownV
      (T.pack $ getProp i propSelects)
      ( \_ txt -> EvAction True $ propSelections %~ setProp i (T.unpack txt)
      )
      (map T.pack os)
      (\v -> hstack [label "Current:", spacer, label v])
      (\v -> label v)

  setProp i txt [] = [(i, txt)]
  setProp i txt (x : xs) = if i == (fst x) then (i, txt) : xs else x : setProp i txt xs

  optRowEd i os =
    textFieldV_
      (T.pack $ intercalate "," os)
      (\t -> EvGenEdits [updateGen $ setPropOpts i (splitOn "," $ T.unpack t)])
      [placeholder optionHint]

  optEdButton i =
    tooltip "Edit property" $
      rowIconStyle $
        button remixEdit2Fill $
          EvAction True $
            toggleVisFlag (VisGenEdit $ EdProperty i)

  oEditing i = (VisGenEdit $ EdProperty i) `elem` vs

genBoxTables :: [VisFlag] -> LineData -> Generator -> YHHNode
genBoxTables vs ld g =
  vstack
    [ hstack
        [ label "Tables" `styleBasic` titleStyle
        , filler
        , tooltip "New table" $
            rowIconStyle $
              button remixAddCircleFill $
                EvGenEdits [updateGen addTable]
        , spacer
        ]
    , spacer
    , vstack $ zipWith (\i tab -> tableRow i tab) [0 ..] (tables g)
    ]
    `styleBasic` boxStyle
 where
  tableRow i table =
    vstack
      [ hstack
          [ tEditBox i
          , widgetIf (tEditing i) $
              textFieldV
                (T.pack $ tabTitle table)
                (\t -> EvGenEdits [updateGen $ setTableTitle i (T.unpack t)])
          , actionRowBox (tableRow' i table) (EvGenerate table) []
          , delBox
              ((VisDelTab i) `elem` vs)
              "Delete table"
              (EvGenEdits [updateGen $ deleteTable i, delVisFlag (VisGenEdit $ EdTable i)])
              (toggleTabDelete i)
          , spacer
          ]
          `styleHover` [bgColor $ darkGray & L.a .~ 0.5]
      , widgetIf (tEditing i) $
          hstack
            [ spacer `styleBasic` [width 20]
            , vstack
                [ flip styleBasic [rangeHeight 50 200] $
                    vscroll $
                      vstack $
                        zipWith (lineRow i) [0 ..] $
                          tabLines table
                , hstack
                    [ spacer `styleBasic` [width 7]
                    , tooltip "New row" $
                        rowIconStyle $
                          button
                            remixAddBoxFill
                            (EvGenEdits [updateGen $ addTableLine i])
                    , spacer
                    , label $
                        T.pack $
                          " Total Weight: " ++ (show $ sum $ map weight $ tabLines table)
                    , filler
                    , popupV_
                        (VisTableHelp `elem` vs)
                        (\b -> if b then EvNull else EvAction False $ toggleVisFlag VisTableHelp)
                        [alignLeft]
                        (label_ tableHelp [multiline] `styleBasic` ((padding 10) : popupStyle))
                    , tooltip "Help" $
                        rowIconStyle $
                          button remixQuestionFill (EvAction False $ toggleVisFlag VisTableHelp)
                    ]
                ]
            , spacer `styleBasic` [width 20]
            ]
      ]

  tEditBox i =
    tooltip "Toggle editing" $
      rowIconStyle $
        button
          (if tEditing i then remixArrowDownSLine else remixArrowRightSLine)
          (EvAction True $ toggleVisFlag (VisGenEdit $ EdTable i))

  tableRow' i table =
    hstack
      [ label (T.pack $ tabTitle table) `nodeVisible` (not $ tEditing i)
      , spacer
      , -- This button sends an EvNull instead of (EvGenerate table) because it's inside
        -- the actionRowBox which sends an EvGenerate anyway. If the button also sent the
        -- event, both would trigger and you'd get two results.
        tooltip "Generate!" $ rowIconStyle $ button remixDie EvNull
      , filler
      ]

  lineRow i n l =
    let thisWeight = weight l
        thisResult = result l
     in hstack
          [ numericFieldV_
              thisWeight
              (\w -> EvGenEdits [updateGen $ changeTableLine i n w thisResult])
              [decimals 0, minValue 1, maxValue 99]
              `styleBasic` [width 30, bgColor transparent, border 0 transparent]
          , separatorLine
          , textFieldV_
              (T.pack thisResult)
              (\r -> EvGenEdits [updateGen $ changeTableLine i n thisWeight (T.unpack r)])
              [placeholder rowHint]
              `styleBasic` [bgColor transparent, border 0 transparent]
          , popupV_
              ((ldTable ld) == i && (ldLine ld) == n && (ldExpand ld))
              (\b -> if b then EvNull else EvAction True $ lineBoxData .~ Nothing)
              [alignLeft]
              ( vstack
                  [ margin 5 $
                      textAreaV
                        (T.pack $ replace "{br}" "\n" $ ldString ld)
                        (\t -> EvAction True $ lineBoxData %~ updateLineDataText (T.unpack t))
                        `styleBasic` [maxWidth 750, maxHeight 300]
                  , spacer
                  , box_ [alignCenter] $
                      hstack
                        [ rowIconStyle $
                            button
                              remixCheckFill
                              ( EvGenEdits
                                  [ updateGen $ changeTableLine i n (weight l) (ldString ld)
                                  , lineBoxData .~ Nothing
                                  ]
                              )
                              `styleBasic` [textSize 24]
                        , spacer
                        , rowIconStyle $
                            button
                              remixCloseFill
                              (EvAction True $ lineBoxData .~ Nothing)
                              `styleBasic` [textSize 24]
                        ]
                  ]
                  `styleBasic` popupStyle
              )
          , if (ldTable ld) == i && (ldLine ld) == n
              then
                hstack
                  [ spacer
                  , tooltip "Expand content" $
                      rowIconStyle $
                        button
                          remixEditBoxFill
                          (EvAction True $ lineBoxData %~ lineDataToggleExpand)
                  , tooltip "Delete row" $
                      rowIconAlertStyle $
                        button remixDeleteBinFill $
                          EvGenEdits [updateGen $ deleteTableLine i n, lineBoxData .~ Nothing]
                  ]
              else
                rowIconStyle $
                  button remixMoreLine $
                    EvAction True $
                      lineBoxData .~ Just (thisWeight, thisResult, i, n, False)
          ]
          `styleBasic` [bgColor $ black & L.a .~ (if odd n then 0.1 else 0.2)]

  tEditing i = (VisGenEdit $ EdTable i) `elem` vs

charBox :: [VisFlag] -> [RPGCharacter] -> YHHNode
charBox vs cs =
  hscroll $
    (flip styleBasic) boxStyle $
      vstack
        [ hstack
            [ label "Characters" `styleBasic` titleStyle
            , filler
            , tooltip "New character" $
                rowIconStyle $
                  button
                    remixAddCircleFill
                    (EvCharsEdit $ characters %~ (++ [(RPGChar "New" [] "")]))
            , spacer
            ]
        , (flip styleBasic) [padding 5] $
            vstack $
              zipWith (\i cha -> charRow i cha) [0 ..] cs
        ]
 where
  charRow i c@(RPGChar cName cFields cNotes) =
    vstack
      [ spacer
      , separatorLine
      , spacer
      , hstack
          [ titledWidgets -- blank title is just so the button aligns better
              ""
              [ tooltip "Toggle notes" $
                  rowIconStyle $
                    button
                      (if cExpanded i then remixArrowDownSLine else remixArrowRightSLine)
                      (EvAction True $ toggleExpandedChar i)
              ]
          , titledWidgets
              "Name"
              [ textFieldV
                  (T.pack cName)
                  (\t -> updateChar i $ c {charName = T.unpack t})
                  `styleBasic` ((rangeWidth 100 200) : inputStyle)
              ]
          , spacer
          , titledWidgets
              "Stats"
              [ textFieldV_
                  (T.pack cFields)
                  (\t -> updateChar i $ c {charFields = T.unpack t})
                  [placeholder statHint]
                  `styleBasic` inputStyle
              ]
          , spacer
          , delBox
              ((VisDelChar i) `elem` vs)
              "Delete character"
              (EvCharDelete i)
              (toggleCharDelete i)
          ]
      , widgetIf (cExpanded i) spacer
      , widgetIf (cExpanded i) $
          flip styleBasic [paddingL 20, paddingR 20] $
            titledWidgets
              "Notes"
              [ textAreaV
                  (T.pack cNotes)
                  (\t -> updateChar i $ c {charNotes = T.unpack t})
                  `styleBasic` ((rangeHeight 50 150) : inputStyle)
              ]
      ]
      `styleBasic` [paddingL 25, paddingR 25]

  updateChar i x = EvCharsEdit $ characters . element i .~ x

  cExpanded i = (VisCharExp i) `elem` vs

handleEvent :: YHHWenv -> YHHNode -> YHHModel -> YHHEvent -> [YHHResp]
handleEvent wenv _ model' evt = case evt of
  EvNull -> []
  EvPathChange fp -> [Task $ EvReload True <$> establishModel fp]
  EvRefresh _ -> [Task $ EvReload True <$> establishModel (model' ^. dataPath)]
  EvReload b m -> [Model $ if b then removeStaleFlags m else m]
  EvAction b f -> [Model $ if b then f model else f model']
  EvSave f -> let m = toggleVisFlag f model in runAndReload (saveFlag f m) m
  EvReactGen -> [Task $ EvReaction <$> newStdGen]
  EvReaction g -> [Model $ model & reaction .~ (Just $ reactRoll g)]
  EvShuffleAll -> [Task $ EvSetImages Nothing <$> newStdGen]
  EvShuffleImage i -> [Task $ EvSetImages (Just i) <$> newStdGen]
  EvSetImages Nothing g -> [Model $ model & images %~ shuffleList g]
  EvSetImages (Just i) g -> [Model $ model & images %~ shuffleAt i g]
  EvNameGen i -> [Task $ EvName i <$> newStdGen]
  EvName i g -> [Model $ model & nameLists . element i %~ nameGen g]
  EvNameBox m -> [Model $ model & namesBoxData .~ m]
  EvNameBoxSave nld -> [Task $ EvReload True <$> saveNameList model' nld]
  EvNameListsNew fn -> [Task $ EvReload False <$> addNewNameList fn model']
  EvRollGen i -> [Task $ EvRoll i <$> newStdGen]
  EvRoll i g -> [Model $ model & dice . element i %~ rollGen (model ^. characters) g]
  EvRollDel i -> let m = model & dice %~ deleteAt i in runAndReload (saveDice m) m
  EvRollChange i t ->
    let m = model & dice . element i . _1 .~ (T.unpack t)
     in runAndReload (saveDice m) m
  EvRollNew -> let m = model & dice %~ ((:) ("d6", [])) in runAndReload (saveDice m) m
  EvGenerate t -> [Task $ EvGenRun t <$> newStdGen]
  EvGenRun t g ->
    [ Model $
        addVisFlag VisGenResults $
          model & genResults %~ T.append (runGen wenv model t g)
    ]
  EvGenLoad t ->
    [Task $ EvGenChange <$> readGeneratorMb ((model ^. dataPath) </> (toGenPath t))]
  -- TODO: Error handling goes here instead of this hacky use of Task
  EvGenChange Nothing -> [Task $ EvGenChange <$> return (Just defGenerator)]
  EvGenChange (Just g) ->
    [ Model $
        removeGenEditFlags $
          delVisFlag VisGenSave $
            model
              & gen .~ g
              & propSelections .~ []
    ]
  EvGenEdits fs -> [Model $ foldr id (addVisFlag VisGenSave model) fs]
  -- TODO: This should delete the old directory if the title's been changed
  EvGenSave ->
    runAndReload
      (writeGenerator (model' ^. dataPath) (model ^. gen))
      (delVisFlag VisGenSave model)
  -- TODO: This won't catch the right directory if the title's been changed
  EvGenDelete -> [Task $ EvRefresh <$> deleteGenerator (model' ^. dataPath) (model ^. gen)]
  EvCharsEdit f -> let m = f model in runAndReload (saveCharacters m) m
  EvCharDelete i ->
    let m = model & characters %~ deleteAt i in runAndReload (saveCharacters m) m
 where
  model = removeStaleFlags model'

  saveDice m = modifyConfig (m ^. dataPath) "dice" (intercalate "," $ map fst $ m ^. dice)

  saveFlag VisNames = saveFlag' "nameBox" VisNames
  saveFlag VisDice = saveFlag' "diceBox" VisDice
  saveFlag VisGens = saveFlag' "genBox" VisGens
  saveFlag VisChars = saveFlag' "characterBox" VisChars
  saveFlag _ = \_ -> return ()

  saveFlag' n f m = modifyConfig (m ^. dataPath) n (show $ getVisFlag f m)

  saveCharacters m = writeCharacters (m ^. dataPath) (m ^. characters)

  runAndReload f m = [Task $ EvReload True <$> (f >> return m)]

establishModel :: FilePath -> IO YHHModel
establishModel fp = do
  initDataPath <- establishDataDir
  createDirectoryIfMissing True $ initDataPath </> fp
  datapaths <- filterM (checkPath initDataPath) =<< listDirectory initDataPath

  g <- newStdGen

  let thisDir = initDataPath </> fp
  conf <- readConfig thisDir
  imageFiles <- establishImages thisDir
  namelists <- establishNameLists thisDir -- TODO: Check if read errors return []
  generators <- readGenerators thisDir -- TODO: Double-check read errors return []
  let gen = head $ generators ++ [defGenerator]
  characters <- readCharacters thisDir -- TODO: Check if read errors return []
  return $
    YHHModel
      thisDir
      (map (initDataPath </>) datapaths)
      True
      Nothing
      []
      Nothing
      ""
      (establishVisFlags conf)
      Nothing
      (shuffleList g imageFiles)
      namelists
      (map (flip (,) "") $ diceStrs conf)
      gen
      (map genTitle generators)
      characters
 where
  checkPath _ ('.' : _) = return False
  checkPath dir p = doesDirectoryExist $ (dir </> p)

establishImages :: FilePath -> IO [[FilePath]]
establishImages dataPath = do
  dirs <- filterM (doesDirectoryExist . (imgDir </>)) =<< listDirectory imgDir
  mapM (establishImagesFromDir . (imgDir </>)) (sort dirs)
 where
  imgDir = dataPath </> "images"

establishImagesFromDir :: FilePath -> IO [FilePath]
establishImagesFromDir imgDir = do
  -- doesFileExist filters out any directories that shouldn't be there
  files <- filterM (doesFileExist . (imgDir </>)) =<< listDirectory imgDir
  return $ map (imgDir </>) files

establishNameLists :: FilePath -> IO [NameList]
establishNameLists dataPath = do
  files <- listDirectory dataPath
  mapM getNameList $ filter (".list" `isSuffixOf`) files
 where
  getNameList f = do
    x <- T.readFile (dataPath </> f)
    return (f, T.pack "", T.lines x)

establishVisFlags :: Config -> [VisFlag]
establishVisFlags conf =
  foldr
    (\(check, flag) acc -> if check conf then flag : acc else acc)
    [VisTray]
    [ (showNames, VisNames)
    , (showDice, VisDice)
    , (showGens, VisGens)
    , (showChars, VisChars)
    ]

saveNameList :: YHHModel -> NameListData -> IO YHHModel
saveNameList model Nothing = return model
saveNameList model (Just (newFP', t, i)) = do
  if newFP /= (nlPath nl) then removeFile oldFP else return ()
  T.writeFile ((model ^. dataPath) </> newFP) t
  reloadNameLists $ model & namesBoxData .~ Nothing
 where
  nl = model ^. nameLists . element i
  oldFP = (model ^. dataPath) </> (nlPath nl)
  newFP = if ".list" `isSuffixOf` newFP' then newFP' else newFP' ++ ".list"

addNewNameList :: FilePath -> YHHModel -> IO YHHModel
addNewNameList fileName model = do
  let filePath = (model ^. dataPath) </> fileName
  check <- doesFileExist filePath
  writeFile filePath ""
  reloadNameLists $
    if check
      then addVisFlag (VisNameError $ "Overwriting '" ++ fileName ++ "'") model
      else model

reloadNameLists :: YHHModel -> IO YHHModel
reloadNameLists model = do
  newNLs <- establishNameLists (model ^. dataPath)
  return $ model & nameLists .~ newNLs

nameGen :: StdGen -> NameList -> NameList
nameGen g (fp, _, ns) = (fp, fTxt, ns)
 where
  fTxt = case listResult g ns of
    Just x -> x
    Nothing -> ""

rollGen :: [RPGCharacter] -> StdGen -> Dice -> Dice
rollGen cs g (str, _) = (str, diceResult cs g str)

getProp :: Int -> [(Int, String)] -> String
getProp _ [] = ""
getProp i ((n, txt) : xs) = if i == n then txt else getProp i xs

runGen :: YHHWenv -> YHHModel -> Table -> StdGen -> T.Text
runGen wenv model table g =
  wrapBy wenv 15 $
    T.pack $
      (tableResult table (tables $ model ^. gen) opts g) ++ "\n--\n\n"
 where
  opts = map snd (model ^. propSelections)

updateLineDataText :: String -> LineData -> LineData
updateLineDataText _ Nothing = Nothing
updateLineDataText s (Just (w, _, i, n, b)) = Just (w, s, i, n, b)

lineDataToggleExpand :: LineData -> LineData
lineDataToggleExpand Nothing = Nothing
lineDataToggleExpand (Just (w, s, i, n, b)) = Just (w, s, i, n, not b)

updateGen :: (Generator -> Generator) -> YHHModel -> YHHModel
updateGen f model = model & gen %~ f

shuffleList :: (Eq a) => StdGen -> [[a]] -> [[a]]
shuffleList _ [] = []
shuffleList g (x : xs) = shuffle i x : shuffleList g' xs
 where
  (i, g') = random g

shuffleAt :: (Eq a) => Int -> StdGen -> [[a]] -> [[a]]
shuffleAt n g xs = xs & element n %~ shuffle (fst $ random g)

-- Doesn't truly shuffle because we only want to pull one random from each,
-- which we just put at the front
shuffle :: (Eq a) => Int -> [a] -> [a]
shuffle _ [] = []
shuffle i xs = let x = xs !! (i `mod` length xs) in x : (delete x xs)
