{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

-- TODO: need to check the above to make sure they're actually required
-- I just copied them from treasurer

module Homunculus.IO.CLI where

import Options.Generic
import System.FilePath
import System.Random

import Homunculus
import Homunculus.Parser (diceResult)

data CLICommand = CLICommand
  { roll :: Maybe String
  , name :: Maybe String
  }
  deriving (Generic, Show)

instance ParseRecord CLICommand

printRollResults :: String -> IO ()
printRollResults s = do
  g <- newStdGen
  putStrLn $ diceResult [] g s

printNameResults :: FilePath -> FilePath -> IO ()
printNameResults dataPath fn = do
  file <- readFile $ dataPath </> fn -- TODO: Existence check
  g <- newStdGen
  putStrLn $
    maybeToString $
      listResult g $
        lines file

maybeToString :: Maybe String -> String
maybeToString (Just s) = s
maybeToString _ = ""
