module Homunculus.IO where

-- This is where general UI functions, mostly IO, go. Especially if they
-- might be used by either the Monomer or CLI versions.

import Control.Exception
import Control.Monad (filterM)
import Data.Char (toLower)
import Data.Ini hiding (options)
import Data.Ini.Reader
import Data.List
import Data.List.Extra (replace)
import Data.List.Split (splitOn)
import Data.Maybe
import System.Directory
import System.FilePath ((</>))

import qualified Data.Ini as D
import qualified Data.Ini.Types as D

import Homunculus.Types

-- Create the dataPath that other functions will use to get everything.
-- If passed Nothing, it returns the "initial" path where all dirs are.
-- If passed a dir, it returns the initial path with that dir appended.
--
-- Could I just use </> "" if I wanted the Nothing option? Probably,
-- at least on some machines. But I don't want to risk it.
establishDataDir :: IO FilePath
establishDataDir = do
  dataPath <- getXdgDirectory XdgConfig "homunculus"
  createDirectoryIfMissing True dataPath
  return dataPath

readConfig :: FilePath -> IO Config
readConfig dataPath = do
  c <- readConfig' dataPath
  return $
    Conf
      { diceStrs = maybe [] (splitOn ",") $ getOption "default" "dice" c
      , showNames = getB True "nameBox" c
      , showDice = getB True "diceBox" c
      , showGens = getB True "genBox" c
      , showChars = getB False "characterBox" c
      }
 where
  getB b field c = maybe b readBool $ getOption "default" field c

  readBool x = (map toLower x) `elem` ["yes", "true", "1"]

readConfig' :: FilePath -> IO (D.Config)
readConfig' dataPath = check =<< catch (readFile $ dataPath </> "homunculus.ini") readHandler
 where
  check ini = case parse ini of
    Right c -> return c
    Left _ -> return emptyConfig

modifyConfig :: FilePath -> String -> String -> IO ()
modifyConfig dataPath name val = do
  c <- readConfig' dataPath
  let items = allItems "default" $ setOption "default" name val c
  writeFile (dataPath </> "homunculus.ini") $ unlines $ map toStr items
 where
  toStr (n, v) = n ++ "=" ++ v

readGenerators :: FilePath -> IO [Generator]
readGenerators dataPath = do
  files <-
    filterM (doesDirectoryExist . (dataPath </>)) =<< listDirectory dataPath
  fmap catMaybes $
    mapM readGeneratorMb $
      map (dataPath </>) $
        filter ("Gen_" `isPrefixOf`) files

readGeneratorMb :: FilePath -> IO (Maybe Generator)
readGeneratorMb genPath = do
  ini <- catch (readFile $ genPath </> "generator.ini") readHandler
  case parse ini of
    Right c ->
      if and [checkField "title" c, checkField "tables" c]
        then do
          let xTitle = getField "title" c
              xProps = filter (`notElem` ["tables", "title"]) $ D.options "default" c
              xTNams = getField "tables" c
          xTabls <- fmap catMaybes $ mapM readTableMb $ splitOn "," $ xTNams
          return $ Just $ Generator xTitle xTabls (map (readOpt c) xProps)
        else return Nothing
    Left _ -> return Nothing
 where
  checkField n c = hasOption "default" n c

  getField n = fromJust . getOption "default" n

  readOpt c o = (o, splitOn "," $ fromJust $ getOption "default" o c)

  readTableMb tn = do
    csv <- catch (readFile $ genPath </> ((toPath tn) ++ ".csv")) readHandler
    if csv == ""
      then return Nothing
      else return $ Just $ Table tn (map toTabLine $ lines csv)

  toTabLine l =
    let (x, y) = break (== ',') l
     in (readInt x, readStr $ drop 1 y)

  readInt s = case reads s of
    [(i, [])] -> i
    _ -> 1

  -- We have to pull the beginning and ending "s (if they exist) before we go through
  -- and start replacing "" with " (as per CSV specification). If we don't, then a
  -- user putting a " at the very start of a line result causes a read error:
  --
  -- CSV: 1,"""quoted text"" unquoted text" becomes 1,\""quoted text\" unquoted text"
  readStr [] = []
  readStr ('"' : xs) = if xs == [] then "" else replace "\"\"" "\"" $ init xs

writeGenerators :: FilePath -> [Generator] -> IO ()
writeGenerators dataPath = mapM_ (writeGenerator dataPath)

-- Could the write function use Data.Ini? Yes
-- Does the write function need to use Data.Ini? No
writeGenerator :: FilePath -> Generator -> IO ()
writeGenerator dataPath gen = do
  deleteGenerator dataPath gen -- Blank slate
  let genDir = dataPath </> generatorDirName gen
  createDirectory genDir

  writeFile
    (genDir </> "generator.ini")
    genFileContents
  if (length $ tables gen) > 0
    then mapM_ (writeTable genDir) $ tables gen
    else return ()
 where
  genFileContents =
    unlines $
      ("title=" ++ (genTitle gen))
        : ("tables=" ++ (intercalate "," $ map tabTitle $ tables gen))
        : (map (\(n, os) -> n ++ "=" ++ (intercalate "," os)) $ options gen)
  writeTable genDir t =
    writeFile (genDir </> tableFileName t) (unlines $ map writeCSV $ tabLines t)

  -- Only using ""s here because that's CSV-compliant, not because it's useful
  writeCSV (w, r) = concat [show w, ",\"", replace "\"" "\"\"" r, "\""]

deleteGenerator :: FilePath -> Generator -> IO ()
deleteGenerator dataPath gen = removePathForcibly $ dataPath </> generatorDirName gen

generatorFileName :: Generator -> FilePath
generatorFileName g = (toPath $ genTitle g) ++ ".gen"

generatorDirName :: Generator -> FilePath
generatorDirName = toGenPath . genTitle

tableFileName :: Table -> FilePath
tableFileName t = (toPath $ tabTitle t) ++ ".csv"

-- Could the write function use Data.Ini? Yes
-- Does the write function need to use Data.Ini? No
writeCharacters :: FilePath -> [RPGCharacter] -> IO ()
writeCharacters dataPath =
  writeFile (dataPath </> "characters.ini") . unlines . concatMap f
 where
  f c =
    [ "[" ++ (charName c) ++ "]"
    , "fields=" ++ (charFields c)
    , "notes=" ++ (replace "\n" "\\" $ charNotes c)
    , "\n"
    ]

readCharacters :: FilePath -> IO [RPGCharacter]
readCharacters dataPath = do
  ini <- catch (readFile $ dataPath </> "characters.ini") readHandler
  case parse ini of
    Right c -> return $ catMaybes $ map (getChar c) $ sections c
    Left _ -> return []
 where
  getChar c n =
    if and [hasOption n "fields" c, hasOption n "notes" c]
      then
        let (Just xFields) = getOption n "fields" c
            (Just xNotes) = getOption n "notes" c
         in Just $ RPGChar n xFields (replace "\\" "\n" xNotes)
      else Nothing

toGenPath :: String -> FilePath
toGenPath = ("Gen_" ++) . toPath

toPath :: String -> FilePath
toPath str = filter (`elem` cs) str
 where
  cs =
    ['A' .. 'Z']
      ++ ['a' .. 'z']
      ++ ['0' .. '9']
      ++ ['_', '-']

-- The real response to invalid entries will be handled in the functions using
-- readHandler so no need to do anything fancy here. But might as well output
-- the error message, just in case.
readHandler :: IOError -> IO String
readHandler e = print e >> return ""
