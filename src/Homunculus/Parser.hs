module Homunculus.Parser where

import Control.Monad (ap)
import Data.Char
import Data.List.Split (splitOn)
import System.Random

import Prelude hiding (
  getChar,
  until,
  (<>),
 )

import Homunculus
import Homunculus.Types

{-
  We're starting out with some functions that aren't actually Parsers but which are tied to them
  by virtue of being the main functions *using* parsers and/or being functions *used by* parsers.
-}

tableResult :: Table -> [Table] -> [String] -> StdGen -> String
tableResult table tabls props g =
  case parse (script tabls' props g') (list !! i) of
    [(x, [])] -> x
    x -> "Error: " ++ (show x)
 where
  list =
    concatMap (uncurry replicate) $
      tabLines table
  tabls' = filter (/= table) tabls -- Prevent infinite loops by chipping away at the table list
  (i, g') = uniformR (0, (length list) - 1) g

diceResult :: [RPGCharacter] -> StdGen -> String -> String
diceResult cs g str =
  case parse (catalogDiceTokens g) str of
    [(xs, [])] ->
      let xRolls = concatMap tokenSums xs
          -- concatMap instead of foldr won't hit all possible operations to be done
          xString = foldr (\t acc -> fullParse $ (tokenStrings t) ++ acc) "" xs
       in if (length xRolls) <= 1 && not (hasCalls xs)
            then "Total: " ++ xString
            else concat ["Result: ", xString, "\nRolls: ", show xRolls]
    x -> "Error: " ++ (show x)
 where
  tokenSums (TokenSum is) = is
  tokenSums _ = []

  tokenStrings (TokenSum is) = show $ sum is
  tokenStrings (TokenCall (n, f)) = show $ forceValFromChars n f cs
  tokenStrings (TokenElse x) = x

  hasCalls [] = False
  hasCalls ((TokenCall _) : _) = True
  hasCalls (_ : xs) = hasCalls xs

  fullParse txt = case parse secondPass txt of
    [(x, [])] -> x
    x -> "OpError: " ++ (show x)

  secondPass = do
    x <- ops <?> getCharAsString
    xs <- many $ secondPass
    return $ concat $ x : xs

-- TODO: Right now you can't nest because splitOn doesn't
-- recognize tokens. Change toFields to something that will.
toFields :: String -> String -> [String]
toFields = splitOn

tableCalls :: [Table] -> [String] -> StdGen -> Parser String
tableCalls tabls props g = do
  x <- (tableCalls' tabls props g1) <> getCharAsString
  xs <- many $ tableCalls tabls props g2
  return $ concat $ x : xs
 where
  (g1, g2) = split g

tableCalls' :: [Table] -> [String] -> StdGen -> Parser String
tableCalls' tabls props g = do
  _ <- string "{table:"
  xs <- throughEndToken
  if xs `elem` (map tabTitle tabls)
    then
      return $
        tableResult
          (head $ filter (\t -> (tabTitle t) == xs) tabls)
          tabls
          props
          g
    else
      return $
        "Error: No table of name \"" ++ xs ++ "\""

{-
  This has to be divided into a number of passes to ensure we still stick to the proper order
  of operations. Tables have to be fully figured out first (which will mean their own call to
  script from within tableCalls) followed by all the dice, followed by everything else.

  The reason dice go next is to that e.g. 3d6+1d4 is calculated (3d6)+(1d4) and not ((3d6)+1)d4
-}

script :: [Table] -> [String] -> StdGen -> Parser String
script tabls props g =
  firstPass
    <?> secondPass g1
    <?> thirdPass g2
 where
  (g1, g2) = split g

  firstPass = tableCalls tabls props g

  secondPass g' =
    let (r1, r2) = split g'
     in do
          x <- (fmap (show . sum) (dice r1)) <> getCharAsString
          xs <- many $ secondPass r2
          return $ concat $ x : xs

  thirdPass g' =
    let (r1, r2) = split g'
     in do
          x <-
            properties props
              <?> list r1
              <?> ops
              <?> upper
              <?> lower
              <?> newline
              <?> getCharAsString
          xs <- many $ thirdPass r2
          return $ concat $ x : xs

catalogDiceTokens :: StdGen -> Parser [DiceToken]
catalogDiceTokens g = many token
 where
  token =
    (fmap TokenSum $ dice g)
      <> (fmap TokenCall $ characterCall)
      <> (fmap TokenElse getCharAsString)

{-
  Parsers are a function which returns (a,String) where
  a is the type of the expected return and the string is
  any remaining text left unparsed.

  Returning [] is equivalent to a total failure to parse.
  Technically I should probably make this :: Maybe (a,String)
  to better represent that, but the list notation is a
  little more convenient to use and allows for the possibility
  of multiple different readings, should that ever be a
  worthwhile feature.
-}
newtype Parser a
  = Parser (String -> [(a, String)])

-- Default (<$) should work, doesn't need redefinition
instance Functor Parser where
  fmap f p = p >>= \x -> return (f x)

instance Monad Parser where
  p1 >>= f = Parser $ \q -> do
    (x, q') <- parse p1 q
    parse (f x) q'
  return x = Parser $ \q -> [(x, q)]

instance MonadFail Parser where
  fail _ = Parser $ \_ -> []

instance Applicative Parser where
  pure = return
  (<*>) = ap

parse :: Parser a -> String -> [(a, String)]
parse (Parser p) str = p str

getChar :: Parser Char
getChar = Parser $ \a -> case a of
  (x : xs) -> [(x, xs)]
  [] -> []

getCharAsString :: Parser String
getCharAsString = Parser $ \a -> case a of
  (x : xs) -> [([x], xs)]
  [] -> []

{-
  Parser combinator. If the first parser is given a string and reads it properly,
  the result is returned as normal. Iff the first parser fails the string is run
  through the second parser instead.
-}

(<>) :: Parser a -> Parser a -> Parser a
p <> p' = Parser $ \q -> case parse p q of
  [] -> parse p' q
  x -> x

{-
  Parser compositor. If the first parser is given a string and reads it properly
  the newly parsed string is fed to the second parser. If the first parser fails
  the second parser is not called.
-}

(<.>) :: Parser String -> Parser String -> Parser String
p <.> p' = Parser $ \q -> case parse p q of
  [(x, [])] -> parse p' x
  x -> x

{-
  Parser combinator/compositor that allows backtracking. Feeds the string to the
  first parser, then takes everything (success or no) and feeds it to the second.
-}
infixr 5 <?>

(<?>) :: Parser String -> Parser String -> Parser String
p <?> p' = Parser $ \q -> case parse p q of
  [(x, xs)] -> parse p' $ x ++ xs
  [] -> parse p' q

satisfy :: (Char -> Bool) -> Parser Char
satisfy f = do
  c <- getChar
  if f c then return c else fail []

char :: Char -> Parser Char
char c = satisfy (== c)

-- This sort of trick is why I use Haskell
string :: String -> Parser String
string = mapM char

-- Convenience functions
alpha, digit :: Parser Char
alpha = satisfy isAlpha
digit = satisfy isDigit

{-
  many and rest both call one another and neither has a base case, meaning in theory
  it can recurse forever. I'm relying on lazy evaluation here.

  many will return a parsed string (using rest) if it can or return a blank string
  otherwise. rest will apply the same parser multiple times, relying on many to
  return a blank string in cases where parsing fails. The final result then gets
  concatenated to bring us to [a].
-}
many :: Parser a -> Parser [a]
many p = rest p <> (return [])

rest :: Parser a -> Parser [a]
rest p = do
  x <- p -- Apply the parser to the first character
  xs <- many p -- Now, using many for safety, apply to the next part
  return $ x : xs -- Return both parts together

until :: String -> Parser String
until s = many $ satisfy (`notElem` s)

oneOf :: [Char] -> Parser Char
oneOf xs = do
  x <- getChar
  if x `elem` xs
    then return x
    else fail []

int :: Parser Int
int = do
  sign <- string "-" <> return "" -- Pull the negative away, if it's there
  digit' <- rest digit
  return (read (sign ++ digit') :: Int)

intList :: Parser [Int]
intList = do
  _ <- char '['
  list <- until "]"
  _ <- char ']'
  return $ map read (splitOn "," list)

throughEndToken :: Parser String
throughEndToken = throughEndToken' 0

throughEndToken' :: Int -> Parser String
throughEndToken' depth
  | depth < 0 = return ""
  | otherwise = do
      xs <- until "{}"
      token <- oneOf "{}"
      xs' <- changeDepth token
      return $ xs ++ xs'
 where
  changeDepth token = do
    xs <-
      throughEndToken' (newDepth token depth)
    if xs == ""
      then return ""
      else return $ token : xs

  newDepth t x = if t == '{' then x + 1 else x - 1

dice :: StdGen -> Parser [Int]
dice g = fudgeDice g <> standardDice g

fudgeDice :: StdGen -> Parser [Int]
fudgeDice g = do
  x <- int <> return 1
  _ <- string "dF"
  return $ take x $ randomRs (-1, 1) g

standardDice :: StdGen -> Parser [Int]
standardDice g = do
  prefix <- int <> return 1
  _ <- char 'd'
  dieSize <- int
  -- TODO: Return to this functionality later
  -- suffix <- string "!>" <> string "!" <> return ""
  return $
    take prefix $
      randomRs (1, dieSize) g

-- Wrapper takes mathemetical operations and converts to string
ops :: Parser String
ops = do
  x <- ops'
  return $ show x

ops' :: Parser Int
ops' = do
  x <- int
  f <- oneOf "+-*/%"
  y <- int
  ret x f y
 where
  ret x f y = case f of
    '+' -> return $ x + y
    '-' -> return $ x - y
    '*' -> return $ x * y
    '/' -> return $ x `div` y -- Integer division
    '%' -> return $ x `mod` y
    _ -> fail []

properties :: [String] -> Parser String
properties props = do
  ('=' : xs) <- properties' props
  return xs

properties' :: [String] -> Parser String
properties' props = do
  _ <- string "{property:"
  xs <- throughEndToken
  return $
    foldr
      ( \(x, y) acc ->
          if (x == "else") || (x `elem` props)
            then y
            else acc
      )
      ""
      (map (break (== '=')) (toFields ";" xs))

list :: StdGen -> Parser String
list g = do
  _ <- string "{list:"
  xs <- throughEndToken
  let items = toFields "|" xs
  return $ items !! (i `mod` length items)
 where
  (i, _) = uniform g

upper :: Parser String
upper = do
  _ <- string "{upper}"
  x <- getChar
  return [toUpper x]

lower :: Parser String
lower = do
  _ <- string "{lower}"
  x <- getChar
  return [toLower x]

newline :: Parser String
newline = do
  _ <- string "{br}"
  return "\n"

characterCall :: Parser (String, String)
characterCall = do
  name <- many $ satisfy isAlpha
  _ <- char '.'
  field <- many $ satisfy isAlpha
  return (name, field)
