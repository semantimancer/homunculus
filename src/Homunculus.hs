module Homunculus where

import Control.Lens
import Data.List (intercalate)
import Data.List.Extra (replace)
import Data.List.Split (splitOn)
import System.Random

import Prelude hiding ((<>))

import Homunculus.Types

--Naive implementation for now
--TODO: Fill out customization options(?), refactor
reactRoll :: StdGen -> String
reactRoll g = (show roll) ++ result
 where
  result
    | roll == 2 = ": Hostile (Immediate)"
    | roll <= 5 = ": Hostile (Startled)"
    | roll <= 8 = ": Uncertain (Considering)"
    | roll <= 11 = ": Favorable (Interested)"
    | otherwise = ": Favorable (Friendly)"
  roll = sum $ take 2 $ randomRs (1, 6) g :: Int

forceValFromChars :: String -> String -> [RPGCharacter] -> Int
forceValFromChars name field cs = case findCharacter name cs of
  Just c -> forceCharValI field c
  Nothing -> 0

findCharacter :: String -> [RPGCharacter] -> Maybe RPGCharacter
findCharacter str = foldr f Nothing
 where
  f x acc =
    if (charName x) == str
      then Just x
      else acc

forceCharVal :: String -> RPGCharacter -> String
forceCharVal str c = case findCharVal str c of
  Just x -> x
  Nothing -> ""

forceCharValI :: String -> RPGCharacter -> Int
forceCharValI str c = case reads $ forceCharVal str c of
  [(i, [])] -> i
  _ -> 0

findCharVal :: String -> RPGCharacter -> Maybe String
findCharVal = (fmap . fmap) fieldVal . findCharStat

findCharStat :: String -> RPGCharacter -> Maybe RPGField
findCharStat str = foldr f Nothing . stringToFields . charFields
 where
  f x acc = if (fieldName x) == str then Just x else acc

fieldName :: RPGField -> String
fieldName = fst . break (== ' ')

fieldVal :: RPGField -> String
fieldVal = f . drop 1 . snd . break (== ' ')
 where
  --  Basically `filter (/= ' ') . takeWhile (/= ':')` in one guaranteed pass
  f [] = []
  f (' ' : xs) = f xs
  f (':' : _) = []
  f (x : xs) = x : f xs

fieldsToString :: [RPGField] -> String
fieldsToString = intercalate ", "

stringToFields :: String -> [RPGField]
stringToFields = splitOn "," . replace ", " ","

defCharacter :: RPGCharacter
defCharacter =
  RPGChar
    "NewCharacter"
    "Strength 18, Dexterity 10"
    "These are some notes"

setGenTitle :: String -> Generator -> Generator
setGenTitle s g = g {genTitle = s}

setPropTitle :: Int -> String -> Generator -> Generator
setPropTitle i t g = g {options = options g & element i . _1 .~ t}

setPropOpts :: Int -> [String] -> Generator -> Generator
setPropOpts i os g = g {options = options g & element i . _2 .~ os}

addProperty :: Generator -> Generator
addProperty g = g {options = (options g) ++ [("New Option", [""])]}

deleteProperty :: Int -> Generator -> Generator
deleteProperty i g = g {options = deleteAt i $ options g}

setTableTitle :: Int -> String -> Generator -> Generator
setTableTitle i t g = g {tables = tables g & element i .~ newTable}
 where
  newTable = (tables g !! i) {tabTitle = t}

addTableLine :: Int -> Generator -> Generator
addTableLine i g = g {tables = tables g & element i %~ addLine}
 where
  addLine t = t {tabLines = (tabLines t) ++ newLine}

addTable :: Generator -> Generator
addTable g = g {tables = (tables g) ++ [Table "New Table" newLine]}

deleteTable :: Int -> Generator -> Generator
deleteTable i g = g {tables = deleteAt i $ tables g}

changeTableLine :: Int -> Int -> Int -> String -> Generator -> Generator
changeTableLine tI lI w s g = g {tables = tables g & element tI %~ changeLine}
 where
  changeLine t = t {tabLines = tabLines t & element lI .~ (w, sanitize s)}
  -- TODO: There's a lot more sanitization that needs to go on here
  sanitize = replace "\n" "{br}"

deleteTableLine :: Int -> Int -> Generator -> Generator
deleteTableLine tI lI g = g {tables = tables g & element tI %~ removeLine}
 where
  removeLine t = t {tabLines = deleteAt lI $ tabLines t}

defGenerator :: Generator
defGenerator =
  Generator
    "New Generator"
    [Table "Table 1" []]
    [("Property #1", [])]

newLine :: [(Int, String)]
newLine = [(1, "")]

propTitle :: Property -> String
propTitle = fst

propValues :: Property -> [String]
propValues = snd

weight :: Line -> Int
weight = fst

result :: Line -> String
result = snd

{-
  GENERAL UTILITY FUNCTIONS
-}

deleteAt :: Int -> [a] -> [a]
deleteAt _ [] = []
deleteAt i (x : xs) =
  if i < 1
    then xs
    else x : deleteAt (i - 1) xs

listResult :: StdGen -> [a] -> Maybe a
listResult _ [] = Nothing
listResult g xs = Just $ xs !! i
 where
  (i, _) = uniformR (0, (length xs) - 1) g
