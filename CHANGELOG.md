# Revision history for Homunculus

0.2.0.0 -- 2024-09-11

* Complete project overhaul (no compatibility with earlier versions)
  * Switched GUI from Gtk to Monomer
  * Internal systems overhauled
    * Parser code upgraded, expected syntax changed
    * Initiative tracker removed
    * Dice map removed
    * Calendar maker removed
    * Character tracker added
  * File handling improved

0.1.0.0 -- 2020-01-24

* Rebuilt project using cabal
