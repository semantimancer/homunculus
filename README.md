# Your Helpful Homunculus

Your Helpful Homunculus is designed to be a tool for GMs to use at the table, during their games.
The goal is to provide a toolbox that assists with bookkeeping and improvisation without getting
in the way of the game.

## Changes Happening!

Moving into v0.2, the code has been completely overhauled and new UI libraries have been chosen.
It will take a good deal of time before everything is properly set up again.
