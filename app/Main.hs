{-# LANGUAGE OverloadedStrings #-}

module Main where

import Monomer
import Options.Generic
import System.FilePath ((</>))

import qualified Monomer.Lens as L

import Homunculus
import Homunculus.IO
import Homunculus.IO.CLI
import Homunculus.IO.Monomer

main :: IO ()
main = run =<< getRecord "Your Helpful Homunculus"

run :: CLICommand -> IO ()
run c = f c =<< establishDataDir
 where
  f (CLICommand (Just r) _) dataPath = printRollResults r
  f (CLICommand _ (Just n)) dataPath = printNameResults (dataPath </> "default") n
  f (CLICommand Nothing Nothing) dataPath = do
    model <- establishModel "default"
    putStrLn dataPath
    startApp model handleEvent buildUI (monomerConfig dataPath)
