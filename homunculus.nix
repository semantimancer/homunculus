{ mkDerivation, base, directory, filepath, monomer, random, lib, text, optparse-generic, split, word-wrap, hsini }:
mkDerivation {
  pname = "homunculus";
  version = "0.2.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base directory filepath monomer random text
    optparse-generic split word-wrap hsini
  ];
  license = lib.licenses.bsd3;
}


